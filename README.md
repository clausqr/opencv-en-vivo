# OpenCV en vivo!

![](cut-opencv-en-vivo.gif)

#### 1. Para bajarlo:
```commandline
git clone https://gitlab.com/clausqr/opencv-en-vivo
cd opencv-en-vivo
```

#### 2. Para recrear el entorno de Anaconda:
```commandline
conda env create --file environment.yaml
```

#### 3. Para correrlo:  
```commandline
conda activate opencv-en-vivo
python opencv-en-vivo.py
```

#### 4. Para usarlo:
1. Copiar imagen con el portapales
2. Clickear "PASTE"
3. Escribir funciones para tocar la imagen, la imagen de entrada está en la variable `src` y la de salida debe terminar asignándose a `dst`.


#### 5. Para desactivar el entorno de Anaconda:
```commandline
conda deactivate
```

