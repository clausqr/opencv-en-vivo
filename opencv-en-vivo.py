import sys
import io
import tkinter as tk
from queue import Queue
from enum import Enum, auto
from threading import Thread
from time import sleep

import numpy as np
import cv2
from PIL import ImageTk, Image

# For compatibility with cv docs:
cv = cv2

placeholder_text = "dst=src; print('replacemen!')"

class EVENTS(Enum):
    miniscript_edit = auto()
    image_grabbed = auto()


class Gui:

    def __init__(self, msg_queue, name):
        self.root = tk.Tk()
        self.root.title(name)
        self.queue = msg_queue
        self.keepalive = True

        self.create_gui()

    def start(self):
        self.root.mainloop()

    # from https://stackoverflow.com/a/25594653
    def on_paste(self):
        self.root.update_idletasks()

        b = None

        try:
            b = bytearray()
            h = ''
            for c in self.root.clipboard_get(type='image/png'):
                if c == ' ':
                    try:
                        b.append(int(h, 0))
                    except Exception as e:
                        print('Exception:{}'.format(e))
                    h = ''
                else:
                    h += c

        except tk.TclError as e:
            b = None
            print('TclError:{}'.format(e))
        finally:
            if b is not None:
                with Image.open(io.BytesIO(b)) as img:
                    print('{}'.format(img))
                    # self.root.label.image = ImageTk.PhotoImage(img.resize((100, 100), Image.LANCZOS))
                    # self.root.label.configure(image=self.root.label.image)
                    # self.set_src(np.array(img))
                    self.queue.put((EVENTS.image_grabbed, np.array(img)))

    def create_main_window(self):
        versions_string = "OpenCV version: {0} \n".format(cv2.__version__) + \
                          "Python version: {0} \n".format(sys.version) + \
                          "numpy version: {0}".format(np.__version__)

        self.root.lblVersion = tk.StringVar()
        self.root.lblVersion.set("Versiones:" + versions_string)
        tk.Label(self.root, textvariable=self.root.lblVersion).pack()

        self.root.paste_button = tk.Button(self.root, text="PASTE", command=self.on_paste)
        self.root.paste_button.pack()

        self.root.entry_field = tk.Text(self.root, height=20, width=80)
        self.root.entry_field.insert(tk.INSERT, placeholder_text)
        self.root.entry_field.pack()
        self.root.entry_field.bind('<Any-KeyPress>', self.miniscript_edited_cb)
        # lambda e:  self.queue.put_nowait((EVENTS.miniscript_edit, self.entry_field.get("1.0", tk.END))))
        self.root.sign = tk.Label(self.root, text='https://gitlab.com/clausqr/opencv-en-vivo/')
        self.root.sign.pack()

        self.root.protocol("WM_DELETE_WINDOW", self.quit_gui)


    def miniscript_edited_cb(self, event):
        # print(event)
        with self.queue.mutex:
            self.queue.queue.clear()
        self.queue.put((EVENTS.miniscript_edit, self.root.entry_field.get("1.0", tk.END)))

    def create_src_window(self):
        window_src = tk.Toplevel(master=self.root)
        window_src.title('src')
        img = ImageTk.PhotoImage(Image.new('RGB', (200, 200)))
        self.root.lblSrc = tk.Label(window_src, image=img)
        self.root.lblSrc.pack()

    def create_dst_window(self):
        window_dst = tk.Toplevel(master=self.root)
        window_dst.title('dst')
        img = ImageTk.PhotoImage(Image.new('RGB', (200, 200)))
        self.root.lblDst = tk.Label(window_dst, image=img)
        self.root.lblDst.pack()

    def create_gui(self):
        self.create_main_window()
        self.create_src_window()
        self.create_dst_window()

    def set_image(self, target, img):
        img_tk = ImageTk.PhotoImage(image=Image.fromarray(img))
        target.config(image=img_tk)
        target.image = img_tk
        target.pack()

    def set_src(self, img):
        self.root.update_idletasks()
        if not self.root.lblSrc.winfo_exists():
            self.create_src_window()

        self.set_image(self.root.lblSrc, img)

    def set_dst(self, img):
        self.root.update_idletasks()
        if not self.root.lblDst.winfo_exists():
            self.create_dst_window()

        self.set_image(self.root.lblDst, img)

    def quit_gui(self):
        self.keepalive = False
        print('Bye!')
        self.root.destroy()


class Process:

    def __init__(self, msg_queue, attached_gui):
        self.src = None
        self.queue = msg_queue
        self.gui = attached_gui
        self.miniscript = None
        self.t = Thread(target=self.watch_queue)
        self.t.daemon = True
        self.t.start()

    def _process_image_grabbed(self, img):
        self.src = np.array(img, dtype=np.uint8)
        self.gui.set_src(self.src)
        self._process_image()

    def _update_miniscript_if_none(self):
        if self.miniscript is None:
            self.miniscript = gui.root.entry_field.get("1.0", tk.END)

    def _process_miniscript_edit(self, miniscript):
        t = gui.root.entry_field.get("1.0", tk.END)
        # FIXME: lo buscamos de nuevo acá porque al capturar el evento no llega a cambiarse, pasar por parametro
        if t != self.miniscript:
            self.miniscript = t
            print("New miniscript:\n" + self.miniscript)
            self._process_image()

    def _process_image(self):

        # catch first run empty
        self._update_miniscript_if_none()

        src = np.copy(self.src)

        try:
            process_function_candidate = self.miniscript
            process_function = compile(process_function_candidate, '<string>', 'exec')
            img = src
            _locals = locals()
            _globals = globals()
            exec(process_function, _globals, _locals)
            if 'dst' in _locals:
                self.dst = _locals['dst']
            else:
                self.dst = np.zeros_like(src)

        except:
            print("Code error:", sys.exc_info()[0])
            self.dst = np.zeros_like(src)

        print("self._external_callback returned ok")
        if self.dst.ndim == 2:
            # "Grayscale"
            self.dst = cv2.cvtColor(self.dst, cv2.COLOR_GRAY2BGR)

        self.gui.set_dst(self.dst)

    def watch_queue(self):
        while True:
            ev = self.queue.get()
            print('Event!')
            if ev[0] == EVENTS.image_grabbed:
                img = ev[1]
                try:
                    self._process_image_grabbed(img)
                except BaseException as e:
                    print('Exception in watch_queue when calling _process_image_grabbed:\n{!r}'.format(e))
            if ev[0] == EVENTS.miniscript_edit:
                miniscript = ev[1]
                try:
                    self._process_miniscript_edit(miniscript)
                except BaseException as e:
                    print('Exception in watch_queue when calling _process_miniscript_edit:\n{!r}'.format(e))

            sleep(0.005)


if __name__ == '__main__':
    # run tkinter in main thread:
    queue = Queue()
    gui = Gui(queue, "OpenCV en vivo!")
    proc = Process(queue, gui)
    gui.start()
